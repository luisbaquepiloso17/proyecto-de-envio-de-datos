package facci.pm.baquejorge.nuevoproyecto;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import facci.pm.baquejorge.nuevoproyecto.rest.adapter.MarketAdapter;
import facci.pm.baquejorge.nuevoproyecto.rest.model.Post;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText EditTextTitle, EditTextDescription, EditTextName;
    Button Btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InitViews();
        Btn.setOnClickListener(this);
    }

    private void InitViews(){
        EditTextTitle = findViewById(R.id.editText);
        EditTextName = findViewById(R.id.editText2);
        EditTextDescription = findViewById(R.id.editText3);

        Btn = findViewById(R.id.button);
    }

    private void PostPosts (){
        MarketAdapter adapter = new MarketAdapter();
        Call <Post> call = adapter.InsertPost(new Post(
                EditTextTitle.getText().toString(),
                EditTextDescription.getText().toString(),
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTieHyVqjXBW31lDElllkvNUW4RVvrFBn60C2dcXa89v-LtdxQtXQ&s.png"
        ));

        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                Log.e("responce",response.body().toString());
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button:
                PostPosts();
                break;
        }
    }
}
